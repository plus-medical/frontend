import React from 'react'
import { Link } from 'react-router-dom'
import './styles.scss'

import { FaRegCheckSquare } from 'react-icons/fa'

function Item ({ id, name, dniType, dni, role, link, photo }) {
  return (
    <Link to={`${link}/${id}`}>
      <li className='item'>
        <div className='item__figure'>
          <img
            className='item__image'
            src={photo}
            alt='photo'
          />
          <FaRegCheckSquare className='item__icon' />
        </div>
        <article className='item__content'>
          <p className='item__name'>{name}</p>
          <p className='item__dni'>{`${dniType} ${dni}`}</p>
          <p className='item__profile'>{role}</p>
        </article>
        <section className='item__indicator' />

      </li>
    </Link>
  )
}

export default Item
