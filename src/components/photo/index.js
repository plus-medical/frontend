import React from 'react'
import './styles.scss'

export default function Photo ({ photo, handleImageUpload }) {
  return (
    <div className='image-upload'>
      <label htmlFor='file-input'>
        <img src={photo} />
      </label>
      <input id='file-input' type='file' accept='image/*' onChange={handleImageUpload} />
    </div>
  )
}
